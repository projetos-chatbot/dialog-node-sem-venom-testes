const express = require("express");
const app = express();
const { WebhookClient } = require("dialogflow-fulfillment");
const { add } = require("lodash");

app.get("/", function (req, res) {
  res.send('<h1 style="color:blue;">✅ ESTOU RODANDO NA PORTA <em style="color:green;">3000<em><h1>');
});

///////////////////////////////////////// Nome Webhook
app.post("/dialogflow", express.json(), function (req, res) {
  const agent = new WebhookClient({ request: req, response: res });
  console.log("Dialogflow Request headers: " + JSON.stringify(req.headers));
  console.log("Dialogflow Request body: " + JSON.stringify(req.body));

  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
  }

  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }

  /////////////////////////////// AQUI INSERE AS FUNÇÕES
  function testeok(agent) {
    for (let i = 1; i <= 5; i++) {
      agent.add(`Funcionando Dialog+Webhook!!! ` + i);
    }
  }

  ///////////////////////////////////////////////// SOMA
  function testeSoma(agent) {
    const num1 = agent.parameters.num1;
    const num2 = agent.parameters.num2;
    agent.add(`A soma é : ${num1 + num2}`);
  }

  /////////////////////// CALCULO SOMA PRODUTOS DELIVERY
  function delivery(agent) {
    //const Tipo_produto = agent.parameters.produto;
    const Tamanho = agent.parameters.tamanho;
    const Quantidade = agent.parameters.quantidade;
    const Sabor = agent.parameters.sabores;
    const Nome = agent.parameters.nome;
    const Telefone = agent.parameters.telefone;
    const Endereco = agent.parameters.endereco;
    const Pagamento = agent.parameters.pagamento;
    const Delivery = agent.parameters.Delivery;
    var total;

    //if (Tipo_produto == "Pizza") {
    if (Tamanho == "Pequena") {
      total = Quantidade * 27;
    } else if (Tamanho == "Média") {
      total = Quantidade * 32;
    } else if (Tamanho == "Grande") {
      total = Quantidade * 37;
    }
    //}

    // DESCRIÇÃO DO SEU PEDIDO
    // response.json({
    //   fulfillmentText:
    agent.add(
      "*DESCRIÇÃO DO SEU PEDIDO:*" +
      "\n" +
      "" +
      "\n" +
      "*Nome:* " +
      Nome +
      "\n" +
      "*Telefone:* " +
      Telefone +
      "\n" +
      "*Endereço:* " +
      Endereco +
      "\n" +
      //"*Tipo produto:* " +
      //Tipo_produto +
      //"\n" +
      "*Quantidade:* " +
      Quantidade +
      "\n" +
      "*Sabor:* " +
      Sabor +
      "\n" +
      "*Tamanho:* " +
      Tamanho +
      "\n" +
      "*Forma de pagamento:* " +
      Pagamento +
      "\n" +
      "*Tipo de entrega:* " +
      Delivery +
      " o pedido" +
      "\n" +
      "" +
      "\n" +
      "*VALOR TOTAL R$:* " +
      total.toFixed(2) +
      "\n" +
      "" +
      "\n" +
      "_Confirmar o pedido?_"
    ); // });
    //response.json({ fulfillmentText: "TEXTO AQUI" });
  }

  ////////////////////////////////////////// CALCULO KELVIN
  function temp(agent) {
    var kelvin = agent.parameters.number + 275.15;
    agent.add("A conversão de Celsius para Kelvin é: " + kelvin);
    // response.json({
    //   fulfillmentText: "A conversão de Celsius para Kelvin é: " + kelvin,
    // });
  }

  /////////////////////////////// CALCULO CELSIUS FAHRENHEIT
  function celsius_fahrenheit(agent) {
    var celsius_fahrenheit = (agent.parameters.number1 * 9) / 5 + 32;

    agent.add(
      "A conversão de Celsius para fahrenheit é: " + celsius_fahrenheit
    );
  }

  /////////////////////////////////////////////// CALCULO IMC
  function CalcIMC(agent) {
    var CalcIMC =
      agent.parameters.peso /
      (agent.parameters.altura * agent.parameters.altura);
    agent.add("O seu resultado de IMC é: " + CalcIMC.toFixed(2));

    if (CalcIMC < 18.5) {
      agent.add(
        "Você está magro com esse índice! \nDigite: *0* _[zero]_ para voltar ao *Menu*"
      );
    } else if (CalcIMC >= 18.5 && CalcIMC < 24.9) {
      agent.add(
        "Você está normal com esse índice! \nDigite: *0* _[zero]_ para voltar ao *Menu*"
      );
    } else if (CalcIMC >= 25 && CalcIMC < 29.9) {
      agent.add(
        "Você está acima do peso com esse índice! \nDigite: *0* _[zero]_ para voltar ao *Menu*"
      );
    } else if (CalcIMC >= 30 && CalcIMC < 39.9) {
      agent.add(
        "Você está com obesidade com esse índice! \nDigite: *0* _[zero]_ para voltar ao *Menu*"
      );
    } else if (CalcIMC > 40) {
      agent.add(
        "Você estácom obesidade grave com esse índice! \nDigite: *0* _[zero]_ para voltar ao *Menu*"
      );
    }
  }
  //////////////////////////////////////////////////

  /////////////////////////////////////// intentMaps
  let intentMap = new Map();
  intentMap.set("Default Welcome Intent", welcome);
  intentMap.set("Default Fallback Intent", fallback);
  intentMap.set("testandoWebHook", testeok);
  intentMap.set("pizza_delivery", delivery);

  intentMap.set("teste-webhook", testeSoma);
  intentMap.set("kelvin", temp);
  intentMap.set("imc", CalcIMC);
  intentMap.set("celsius_fahrenheit", celsius_fahrenheit);

  // intentMap.set('your intent name here', yourFunctionHandler);
  // intentMap.set('your intent name here', googleAssistantHandler);
  agent.handleRequest(intentMap);
});

let port = 3000;
app.listen(port, () => {
  console.log("Executando na porta: " + port);
});
